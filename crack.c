#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=34;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char *crackHash(char *target, char *dictionary)
{
    FILE *dict = fopen(dictionary, "r");// Open the dictionary file
    if (dict == NULL)
    {  
        printf("%s is not readable.\n", dictionary);
        exit(1);
        
    }
    char *lookup = (char *) malloc(sizeof(char)*PASS_LEN);  // Loop through the dictionary file, one line at a time
    while(fgets(lookup, PASS_LEN, dict)!= NULL)
    {
        char *n1 = strchr(lookup, '\n');
        if (n1 != NULL)
        {
            *n1 = '\0';
        }
        // Hash each password. Compare to the target hash.
        // If they match, return the corresponding password.
        char *hash = md5(lookup, strlen(lookup));
        if(strcmp(hash, target)==0)
        {       
            free(hash);// Free up memory?
            return lookup;
        }
    }        
    fclose(dict);
}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    // Open the hash file for reading.
    FILE *hashf = fopen (argv[1], "r");
    if(hashf == NULL)
    {
        printf("%s is not readable", argv[1]);
        exit(1);
        
    }
    char pass[50];
    while(fgets(pass, 50, hashf) != NULL)
    {
        char *n1 = strchr(pass, '\n');
        if(n1 != NULL)
        {
            *n1 = '\0';
        }
        char *password = crackHash(pass, argv[2]);// For each hash, crack it by passing it to crackHash
        printf ("%s %s\n", pass, password);// Display the hash along with the cracked password:
        free(password);// Free up any malloc'd memory?
        
    }
    fclose(hashf);// Close the hash file
    return 0;
}
    
    
    
   




    // Open the hash file for reading.
    

    // For each hash, crack it by passing it to crackHash
    
    // Display the hash along with the cracked password:
    //   5d41402abc4b2a76b9719d911017c592 hello
    
    // Close the hash file
    
    // Free up any malloc'd memory?

